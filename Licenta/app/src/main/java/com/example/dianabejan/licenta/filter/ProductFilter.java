package com.example.dianabejan.licenta.filter;
public class ProductFilter {

    private String category, name;
    private int priceMin, priceMax;

    public ProductFilter(final String category, final String name, final int priceMin, final int priceMax) {
        this.category = category;
        this.name = name;
        this.priceMin = priceMin;
        this.priceMax = priceMax;
    }

    @Override
    public String toString() {
        return "" + category +
                "/" + name +
                "/" + priceMin +
                "/" + priceMax +
                '/';
    }

    public String getCategory() {
        return category;
    }

    public String getName() {
        return name;
    }

    public int getPriceMin() {
        return priceMin;
    }

    public int getPriceMax() {
        return priceMax;
    }
}
