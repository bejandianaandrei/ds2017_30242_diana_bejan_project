package com.example.dianabejan.licenta.services;

import com.example.dianabejan.licenta.models.Color;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ColorService {
    private final static String appName = "color/";
    private final StringBuilder colorRawJSON = new StringBuilder();
    private final String serverHost;

    public ColorService(final String serverHost) {
        this.serverHost = serverHost;
    }

    public Color get(final int key) {
        try {
            final URL url = new URL(serverHost + appName + key);
            final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                colorRawJSON.append(line);
            }
            rd.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }
        try {

            final JSONObject jsonObject =  new JSONObject(colorRawJSON.toString());
            final String name = jsonObject.optString("name");
            final String value = jsonObject.optString("value");
            final int id = jsonObject.optInt("id");

            return new Color(id, value, name);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
