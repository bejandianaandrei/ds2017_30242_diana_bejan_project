package com.example.dianabejan.licenta.models;

public class ProductMeta {
    private Integer id;
    private String photo;
    private Color color;
    private Material material;

    public ProductMeta(Integer id, String photo, Color color, Material material) {
        this.id = id;
        this.photo = photo;
        this.color = color;
        this.material = material;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(final Color color) {
        this.color = color;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(final Material material) {
        this.material = material;
    }
}