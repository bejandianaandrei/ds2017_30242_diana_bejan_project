package com.example.dianabejan.licenta;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        final Button categoriesButton = findViewById(R.id.categories_button);

        categoriesButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Intent intent= new Intent(MainActivity.this,CategoriesActivity.class);
                startActivity(intent);
            }
        });
    }
}
