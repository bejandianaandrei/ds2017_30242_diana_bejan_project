package com.example.dianabejan.licenta.services;

import com.example.dianabejan.licenta.models.Category;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CategoriesService {

    private final static String appName = "category";
    private final StringBuilder categoryRawJSON = new StringBuilder();

    public CategoriesService(final String serverHost) {
        try {
            final URL url = new URL(serverHost + appName);
            final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                categoryRawJSON.append(line);
            }
            rd.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    public String[] get() {
        String categories[] = {};
        try {
            final JSONArray categoryJsonArray = new JSONArray(categoryRawJSON.toString());
            final List<String> categs = new ArrayList<>();
            for (int i = 0; i < categoryJsonArray.length(); ++i) {
                final JSONObject jsonObject = categoryJsonArray.getJSONObject(i);
                final String name = jsonObject.optString("name");

                categs.add(name);
            }
            if (categs.size() == 0){
                categs.add(Category.DEFAULT.getName());
            }
            categories = Arrays.copyOf(categs.toArray(), categs.size(), String[].class);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return categories;
    }
}
