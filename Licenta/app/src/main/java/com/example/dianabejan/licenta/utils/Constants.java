package com.example.dianabejan.licenta.utils;

public class Constants {
    public static final String SHOPPY_CATEG = "com.app.shoppy.category";
    public static final String SHOPPY_PRODUCT = "com.app.shoppy.product";

    public static final String PRODUCT_LABEL = "product_label";
    public static final String PRODUCT_PICTURE = "product_picture";
    public static final String PRODUCT_ID_HIDDEN = "product_id_hidden";

    public static final String PRODUCT_PRICE = "product_price";
    public static final String PRODUCT_PRICE_ORIGINAL = "product_price_original";

    public static final String CURRENCY = "RON";
}
