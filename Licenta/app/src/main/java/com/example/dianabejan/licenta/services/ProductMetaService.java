package com.example.dianabejan.licenta.services;

import com.example.dianabejan.licenta.models.ProductMeta;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProductMetaService {
    private final static String appName = "product_meta/";
    private final StringBuilder productRawJSON = new StringBuilder();
    private final String serverHost;

    public ProductMetaService(final String serverHost) {
        this.serverHost = serverHost;
    }

    public ProductMeta[] get(final int key) {
        try {
            final URL url = new URL(serverHost + appName + key);
            final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                productRawJSON.append(line);
            }
            rd.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }

        final List<ProductMeta> products = new ArrayList<>();

        try {
            JSONArray jsonArray = new JSONArray(productRawJSON.toString());
            for (int i = 0; i < jsonArray.length(); ++i) {
                final JSONObject jsonObject = jsonArray.getJSONObject(i);
                final String photo = jsonObject.optString("photo");
                final int colorId = jsonObject.optInt("color");
                final int materialId = jsonObject.optInt("material");
                final int id = jsonObject.optInt("id");

                products.add(new ProductMeta(id, photo, new ColorService(serverHost).get(colorId), new MaterialService(serverHost).get(materialId)));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return Arrays.copyOf(products.toArray(), products.size(), ProductMeta[].class);
    }
}
