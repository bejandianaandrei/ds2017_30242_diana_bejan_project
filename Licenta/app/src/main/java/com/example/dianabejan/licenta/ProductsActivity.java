package com.example.dianabejan.licenta;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.example.dianabejan.licenta.models.Product;
import com.example.dianabejan.licenta.filter.ProductFilter;
import com.example.dianabejan.licenta.services.ProductsService;
import com.example.dianabejan.licenta.utils.Constants;
import com.example.dianabejan.licenta.utils.DownloadImageTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProductsActivity extends AppCompatActivity {
    private ProductsService productsService ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        final String category = getIntent().getStringExtra(Constants.SHOPPY_CATEG);
        final ProductFilter filter = new ProductFilter(category,"*", 0, 0);
        productsService = new ProductsService(getResources().getString(R.string.server_url));
        final List<Product> products = productsService.get(filter);
        final List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();

        for (final Product product: products) {
            HashMap<String, String> hm = new HashMap<String, String>();
            hm.put(Constants.PRODUCT_LABEL, product.getName());

            hm.put(Constants.PRODUCT_PICTURE, product.getPhoto());
            final int price = product.getPrice();
            final int originalPrice = product.getOriginalPrice();

            hm.put(Constants.PRODUCT_PRICE, price + Constants.CURRENCY);
            hm.put(Constants.PRODUCT_PRICE_ORIGINAL, originalPrice + Constants.CURRENCY);
            hm.put(Constants.PRODUCT_ID_HIDDEN, "" + product.getId());

            aList.add(hm);
        }

        final String[] from = {Constants.PRODUCT_LABEL, Constants.PRODUCT_PRICE, Constants.PRODUCT_PRICE_ORIGINAL, Constants.PRODUCT_PICTURE, Constants.PRODUCT_ID_HIDDEN};
        int[] to = {R.id.product_label, R.id.product_price, R.id.product_price_original, R.id.product_picture_url, R.id.product_id_hidden};

        final SimpleAdapter simpleAdapter = new SimpleAdapter(getBaseContext(), aList, R.layout.content_products, from, to) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                ConstraintLayout layout = ((ConstraintLayout) super.getView(position, convertView, parent));
                TextView price = (TextView) layout.getViewById(R.id.product_price);
                TextView aprice = (TextView) layout.getViewById(R.id.product_price_original);
                TextView url = (TextView) layout.getViewById(R.id.product_picture_url);
                new DownloadImageTask((ImageView) layout.getViewById(R.id.product_picture))
                        .execute(getResources().getString(R.string.server_url) + url.getText().toString());
                if (!price.getText().toString().equals(aprice.getText().toString())) {
                    price.setTextColor(Color.RED);
                } else {
                    aprice.setTextColor(Color.GREEN);
                    price.setVisibility(View.INVISIBLE);
                }
                return layout;
            }
        };

        final ListView androidListView = (ListView) findViewById(R.id.product_list);
        androidListView.setAdapter(simpleAdapter);
        setListener(androidListView);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void setListener(final ListView listView) {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Intent intent= new Intent(ProductsActivity.this, ProductViewActivity.class);
                intent.putExtra(Constants.SHOPPY_PRODUCT, ((HashMap<String, String>)parent.getAdapter().getItem(position)).get(Constants.PRODUCT_ID_HIDDEN));
                startActivity(intent);
            }
        });
    }

}
