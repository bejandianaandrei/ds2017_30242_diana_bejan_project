package com.example.dianabejan.licenta;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.dianabejan.licenta.services.CategoriesService;
import com.example.dianabejan.licenta.utils.Constants;

public class CategoriesActivity extends AppCompatActivity {
    private static String[] categoryArray;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setTitle("Categories");
        final CategoriesService categoriesService = new CategoriesService(getResources().getString(R.string.server_url));
        categoryArray = categoriesService.get();
        super.onCreate(savedInstanceState);

        setContent();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        setContent();
    }

    private void setContent(){
        setContentView(R.layout.activity_categories);

        final ArrayAdapter adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, categoryArray);

        final ListView listView = (ListView) findViewById(R.id.category_list);
        listView.setAdapter(adapter);
        listView.setClickable(true);
        setListener(listView);

    }

    public void setListener(final ListView listView) {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Intent intent= new Intent(CategoriesActivity.this,ProductsActivity.class);
                intent.putExtra(Constants.SHOPPY_CATEG,parent.getAdapter().getItem(position).toString());
                startActivity(intent);
            }
        });
    }
}
