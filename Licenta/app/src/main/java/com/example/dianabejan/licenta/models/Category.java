package com.example.dianabejan.licenta.models;

import java.time.LocalDateTime;

public class Category {
    public static final Category DEFAULT = new Category(-1, "Not found", LocalDateTime.now(), null);
    private int ID;
    private String name;
    private LocalDateTime createdDate;
    private LocalDateTime deletedDate;

    public Category(final int ID, final String name, final LocalDateTime createdDate, final LocalDateTime deletedDate) {
        this.ID = ID;
        this.name = name;
        this.createdDate = createdDate;
        this.deletedDate = deletedDate;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(LocalDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

}