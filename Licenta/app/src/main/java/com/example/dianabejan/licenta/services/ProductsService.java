package com.example.dianabejan.licenta.services;

import com.example.dianabejan.licenta.models.Product;
import com.example.dianabejan.licenta.filter.ProductFilter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ProductsService {

    private final static String appName = "product/";
    private final StringBuilder productRawJSON = new StringBuilder();
    private final String serverHost;

    public ProductsService(final String serverHost) {
        this.serverHost = serverHost;
    }

    public List<Product> get(final ProductFilter filter) {
        try {
            final URL url = new URL(serverHost + appName + filter.toString());
            final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                productRawJSON.append(line);
            }
            rd.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }
        List<Product> products = new ArrayList<>();
        try {
            final JSONArray productJsonArray = new JSONArray(productRawJSON.toString());
            for (int i = 0; i < productJsonArray.length(); ++i) {
                final JSONObject jsonObject = productJsonArray.getJSONObject(i);
                final String name = jsonObject.optString("name");
                final String photo = jsonObject.optString("product_picture");
                final String description = jsonObject.optString("description");
                final String category = jsonObject.optString("category");
                final int price = jsonObject.optInt("price");
                final int id = jsonObject.optInt("id");

                final int originalPrice = jsonObject.optInt("original_price");
                final LocalDateTime createdDate = LocalDateTime.parse(jsonObject.optString("created_date"), DateTimeFormatter.ISO_ZONED_DATE_TIME);

                final Product product = new Product(id, photo, name, description, category, price, originalPrice, createdDate);
                products.add(product);
            }

            //products = Arrays.copyOf(prods.toArray(), prods.size(), Product[].class);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return products;
    }

    public Product get(final int key) {
        try {
            final URL url = new URL(serverHost + appName + key);
            final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                productRawJSON.append(line);
            }
            rd.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }
        try {

                final JSONObject jsonObject =  new JSONObject(productRawJSON.toString());
                final String name = jsonObject.optString("name");
                final String photo = jsonObject.optString("product_picture");
                final String description = jsonObject.optString("description");
                final String category = jsonObject.optString("category");
                final int price = jsonObject.optInt("price");
                final int id = jsonObject.optInt("id");

                final int originalPrice = jsonObject.optInt("original_price");
                final LocalDateTime createdDate = LocalDateTime.parse(jsonObject.optString("created_date"), DateTimeFormatter.ISO_ZONED_DATE_TIME);

                return new Product(id, photo, name, description, category, price, originalPrice, createdDate);

            //products = Arrays.copyOf(prods.toArray(), prods.size(), Product[].class);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}