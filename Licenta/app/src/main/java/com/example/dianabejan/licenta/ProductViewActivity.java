package com.example.dianabejan.licenta;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.example.dianabejan.licenta.models.Product;
import com.example.dianabejan.licenta.models.ProductMeta;
import com.example.dianabejan.licenta.services.ProductMetaService;
import com.example.dianabejan.licenta.services.ProductsService;
import com.example.dianabejan.licenta.utils.Constants;
import com.example.dianabejan.licenta.utils.DownloadImageTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProductViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        displayProduct();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }
    public void displayProduct() {
        final int id = Integer.parseInt(getIntent().getStringExtra(Constants.SHOPPY_PRODUCT));
        final ProductMetaService metaService = new ProductMetaService(getResources().getString(R.string.server_url));
        final ProductsService productService = new ProductsService(getResources().getString(R.string.server_url));
        final ProductMeta[] productMeta = metaService.get(id);
        final Product product = productService.get(id);

        final List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();
        ((TextView)findViewById(R.id.view_product_label)).setText(product.getName());

        HashMap<String, String> hm = new HashMap<String, String>();

        hm.put(Constants.PRODUCT_PICTURE, product.getPhoto());
        aList.add(hm);

        for(ProductMeta meta : productMeta) {
            HashMap<String, String> map = new HashMap<>();
            map.put(Constants.PRODUCT_PICTURE, meta.getPhoto());
            aList.add(map);
        }

        final String[] from = {Constants.PRODUCT_PICTURE};
        int[] to = {R.id.product_image_url};

        final SimpleAdapter simpleAdapter = new SimpleAdapter(getBaseContext(), aList, R.layout.content_product_view, from, to) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                ConstraintLayout layout = ((ConstraintLayout) super.getView(position, convertView, parent));

                TextView url = (TextView) layout.getViewById(R.id.product_image_url);
                new DownloadImageTask((ImageView) layout.getViewById(R.id.product_image_item))
                        .execute(getResources().getString(R.string.server_url) + url.getText().toString());
                return layout;
            }
        };

        final ListView androidListView = (ListView) findViewById(R.id.product_image_list);
        androidListView.setAdapter(simpleAdapter);

    }

}
