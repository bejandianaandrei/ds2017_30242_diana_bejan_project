package com.example.dianabejan.licenta.services;

import com.example.dianabejan.licenta.models.Material;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MaterialService {
    private final static String appName = "material/";
    private final StringBuilder materialRawJSON = new StringBuilder();
    private final String serverHost;

    public MaterialService(final String serverHost) {
        this.serverHost = serverHost;
    }

    public Material get(final int key) {
        try {
            final URL url = new URL(serverHost + appName + key);
            final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                materialRawJSON.append(line);
            }
            rd.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }
        try {
            final JSONObject jsonObject =  new JSONObject(materialRawJSON.toString());
            final String name = jsonObject.optString("name");
            final String description = jsonObject.optString("description");
            final int id = jsonObject.optInt("id");

            return new Material(id, name, description);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
