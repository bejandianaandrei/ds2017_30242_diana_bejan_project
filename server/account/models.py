from django.conf import settings
from django.utils import timezone

from django.db import models
from django.contrib.auth.models import AbstractUser

from account import validators


class User(AbstractUser):
    birthday = models.DateField(null=True, blank=True)
    registration_date = models.DateTimeField(null=True, blank=True)
    deleted_date = models.DateTimeField(null=True, blank=True)
    last_login = models.DateTimeField(null=True)
    photo = models.ImageField(upload_to='account_pictures/', blank=True, null=True)


class Address(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='address_user')
    country = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    address = models.CharField(max_length=100)


class Color(models.Model):
    name = models.CharField(max_length=50)
    value = models.CharField(max_length=8, default="#000000")

    def get_name(self, obj):
        return obj.name


class Size(models.Model):
    name = models.CharField(max_length=50)
    value = models.SmallIntegerField(default=0)


class Material(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=256)


class Category(models.Model):
    name = models.CharField(max_length=50)
    created_date = models.DateTimeField(default=timezone.now())
    deleted_date = models.DateTimeField(blank=True, null=True)

    def __iter__(self):
        return [field.value_to_string(self) for field in Category._meta.fields]


    class Meta:
        verbose_name_plural = "Categories"
        ordering = ('name',)


class Product(models.Model):
    product_picture = models.ImageField(upload_to='product_pictures/', blank=True, null=True)
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=512)
    category = models.ForeignKey(Category, related_name='product_category', on_delete=models.CASCADE, default=1)
    price = models.FloatField(default=0.0)
    original_price = models.FloatField(default=0.0)
    created_date = models.DateTimeField(default=timezone.now())
    deleted_date = models.DateTimeField(blank=True, null=True)

    def __iter__(self):
        return [field.value_to_string(self) for field in Product._meta.fields]

    def get_product_picture(self):
        return settings('MEDIA_ROOT') + self.product_picture


class ProductMeta(models.Model):
    photo = models.ImageField(upload_to='product_pictures/', blank=True, null=True)
    color = models.ForeignKey(Color, related_name='product_color', on_delete=models.CASCADE, blank=True, null=True)
    material = models.ForeignKey(Material, related_name='product_material', on_delete=models.CASCADE, blank=True, null=True)
    product = models.ForeignKey(Product, related_name='product_meta', on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        unique_together = (('color', 'material', 'product'),)


class Stock(models.Model):
    product_meta = models.ForeignKey(ProductMeta, related_name='product_stock', on_delete=models.CASCADE)
    size = models.ForeignKey(Size, related_name='product_size', on_delete=models.CASCADE, blank=True, null=True)
    quantity = models.IntegerField(default=0)


class Order(models.Model):
    user = models.ForeignKey(User, related_name='user_order', on_delete=models.CASCADE)
    product = models.ForeignKey(ProductMeta, related_name='product_order', on_delete=models.CASCADE)
    address = models.ForeignKey(Address, related_name='address_order', on_delete=models.CASCADE)
    quantity = models.IntegerField(default=0)
    created_date = models.DateTimeField(default=timezone.now())
    deleted_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ('created_date',)


class CartProduct(models.Model):
    user = models.ForeignKey(User, related_name='user_cart', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name='product_cart', on_delete=models.CASCADE)
    stock = models.ForeignKey(Stock, related_name='stock_cart', on_delete=models.CASCADE, blank=True, null=True)
    added_date = models.DateTimeField(default=timezone.now())
    quantity = models.IntegerField(default=1)

    class Meta:
        unique_together = (('user', 'product'),)
        ordering = ('added_date',)


class Review(models.Model):
    user = models.ForeignKey(User, related_name='user_review', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name='product_review', on_delete=models.CASCADE)
    size = models.ForeignKey(Size, related_name='size_review', on_delete=models.CASCADE)
    added_date = models.DateTimeField(default=timezone.now())
    height = models.IntegerField(default=150, validators=[validators.validate_height])
    weight = models.IntegerField(default=50, validators=[validators.validate])
    bust = models.IntegerField(default=50, validators=[validators.validate])
    waist = models.IntegerField(default=50, validators=[validators.validate])
    hips = models.IntegerField(default=50, validators=[validators.validate])

    class Meta:
        unique_together = (('user', 'product'),)
        ordering = ('added_date',)
