import csv
from _decimal import InvalidOperation, Decimal

import numpy
import pandas as pandas
from PIL import Image
from django.conf import settings
from django.contrib.auth import authenticate
from django.http import Http404, HttpResponse
from django.templatetags.static import static
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView
from rest_framework import generics, permissions, status
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
    HTTP_204_NO_CONTENT,
    HTTP_201_CREATED
)
from rest_framework.views import APIView
from sklearn import svm

from account.models import Category, Color, Size, Material, Product, ProductMeta, Stock, Order, CartProduct, Review
from account.models import User, Address
from account.serializers import CategorySerializer, ColorSerializer, SizeSerializer, MaterialSerializer, \
    ProductSerializer, ProductMetaSerializer, StockSerializer, OrderSerializer, CartProductSerializer, ReviewSerializer
from account.serializers import UserSerializer, AddressSerializer


@permission_classes((AllowAny,))
def imageView(request, imagePath):
    response = HttpResponse(content_type="image/jpeg")
    img = Image.open("product_pictures/" + imagePath)
    img.save(response, 'jpeg')
    return response


@permission_classes((AllowAny,))
class ProductListView(APIView):

    def get_objects(self, category, name_filter, price_min, price_max):
        try:
            filters = dict()
            if category != "*":
                filters["category__name"] = category
            if name_filter != "*":
                filters["name__icontains"] = name_filter

            if price_min != "0" or price_max != "0":
                filters["price__gte"] = Decimal(price_min)
                filters["price__lte"] = Decimal(price_max)
            filters["deleted_date"] = None
            return Product.objects.filter(**filters)
        except InvalidOperation:
            raise Http404

    @permission_classes(AllowAny)
    def get(self, request, category, name_filter, price_min, price_max):
        products = self.get_objects(category, name_filter, price_min, price_max)
        serializer = ProductSerializer(products, many=True)
        return Response(serializer.data)

@permission_classes((AllowAny,))
class ProductDetailView(APIView):

    def get(self, request, pk):
        product = Product.objects.filter(pk=pk)
        serializer = ProductSerializer(product[0])
        return Response(serializer.data)

    def delete(self, request, pk):
        product = Product.objects.filter(pk=pk)
        product2 = Product.objects.filter(pk=pk)
        product2.deleted_date = timezone.now()
        serializer = ProductSerializer(product, product2)
        serializer.save()
        return Response(serializer.data)

@permission_classes((AllowAny,))
class ProductMetaListView(APIView):

    def get_objects(self, category, color, material):
        try:
            filters = dict()
            if category != "*":
                filters["category__name"] = category
            if color != "*":
                filters["color_name"] = color

            if material != "*":
                filters["material_name"] = material
            filters["product__deleted_date"] = None
            return ProductMeta.objects.filter(**filters)
        except InvalidOperation:
            raise Http404

    def get(self, request, category, color, material):
        products = self.get_objects(category, color, material)
        serializer = ProductMetaSerializer(products, many=True)
        return Response(serializer.data)

@permission_classes((AllowAny,))
class ProductMetaDetailView(APIView):

    def get_objects(self, product_id):
        try:
            return ProductMeta.objects.filter(product__id=product_id, product__deleted_date=None)
        except InvalidOperation:
            raise Http404

    def get(self, request, product_id):
        products = self.get_objects(product_id)
        serializer = ProductMetaSerializer(products, many=True)
        return Response(serializer.data)


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    username = request.data.get("username")
    password = request.data.get("password")
    if username is None or password is None:
        return Response({'error': 'Please provide both username and password'},
                        status=HTTP_400_BAD_REQUEST)
    user = authenticate(username=username, password=password)
    if not user:
        return Response({'error': 'Invalid Credentials'},
                        status=HTTP_404_NOT_FOUND)
    u = User.objects.all().filter(username=username, deleted_date=None)
    if not u:
        return Response({'error': 'Invalid Credentials'},
                        status=HTTP_404_NOT_FOUND)
    token, _ = Token.objects.get_or_create(user=user)
    return Response({'token': token.key},
                    status=HTTP_200_OK)


class ActiveUserList(generics.ListCreateAPIView):
    queryset = User.objects.all().filter(deleted_date=None)
    serializer_class = UserSerializer


class UserList(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = UserSerializer

    def get_queryset(self):
        return User.objects.all().filter(id=self.kwargs.get('pk', None), deleted_date=None)


class AddressView(generics.ListCreateAPIView):
    serializer_class = AddressSerializer
    queryset = Address.objects.all()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user,
                        country=self.request.data.get('country', ''),
                        city=self.request.data.get('city', ''),
                        address=self.request.data.get('address', ''))
        return Response(serializer.data)

    def get(self, request):
        addresses = Address.objects.filter(user=request.user)
        serializer = AddressSerializer(addresses, many=True)
        return Response(serializer.data)


class AddressDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Address.objects.all()
    serializer_class = AddressSerializer

    def get(self, request, pk):
        address = Address.objects.filter(user=request.user, pk=pk)
        serializer = AddressSerializer(address)
        return Response(serializer.data)


@permission_classes((AllowAny,))
class CategoryView(generics.ListCreateAPIView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()

    def perform_create(self, serializer):
        serializer.save(name=self.request.data.get('name', ''))
        return Response(serializer.data)

    def get(self, request):
        categories = Category.objects.filter(deleted_date=None)
        serializer = CategorySerializer(categories, many=True)
        return Response(serializer.data)


@permission_classes((AllowAny,))
class CategoryDetailView(generics.RetrieveDestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def get_object(self, pk):
        try:
            return Category.objects.get(pk=pk)
        except Category.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        category = self.get_object(pk)
        serializer = CategorySerializer(category)
        return Response(serializer.data)

    def delete(self, request, pk):
        category = self.get_object(pk)
        category.deleted_date = timezone.now()
        serializer = CategorySerializer(category, data=category.__dict__)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@permission_classes((AllowAny,))
class ColorView(generics.ListCreateAPIView):
    serializer_class = ColorSerializer

    def perform_create(self, serializer):
        serializer.save(name=self.request.data.get('name', ''),
                        value=self.request.data.get('value', '#000000'))
        return Response(serializer.data)

    def get(self, request):
        colors = Color.objects.all()
        serializer = ColorSerializer(colors, many=True)
        return Response(serializer.data)

@permission_classes((AllowAny,))
class ColorDetailView(generics.RetrieveAPIView):
    serializer_class = ColorSerializer
    queryset = Color.objects.all()

    def get(self, request, pk):
        color = Color.objects.get(pk=pk)
        serializer = ColorSerializer(color)
        return Response(serializer.data)

@permission_classes((AllowAny,))
class SizeView(APIView):
    serializer_class = SizeSerializer

    def perform_create(self, serializer):
        serializer.save()

    def get(self, request):
        sizes = Size.objects.all()
        serializer = SizeSerializer(sizes, many=True)
        return Response(serializer.data)

@permission_classes((AllowAny,))
class SizeDetailView(generics.RetrieveAPIView):
    serializer_class = SizeSerializer
    queryset = Size.objects.all()

    def get(self, request, pk):
        size = Size.objects.get(pk=pk)
        serializer = SizeSerializer(size)
        return Response(serializer.data)

@permission_classes((AllowAny,))
class MaterialView(APIView):
    serializer_class = MaterialSerializer

    def perform_create(self, serializer):
        serializer.save()

    def get(self, request):
        materials = Material.objects.all()
        serializer = MaterialSerializer(materials, many=True)
        return Response(serializer.data)

@permission_classes((AllowAny,))
class MaterialDetailView(generics.RetrieveAPIView):
    serializer_class = MaterialSerializer
    queryset = Material.objects.all()

    def get(self, request, pk):
        material = Material.objects.get(pk=pk)
        serializer = MaterialSerializer(material)
        return Response(serializer.data)

@permission_classes((AllowAny,))
class StockDetailView(APIView):
    serializer_class = StockSerializer

    def get_queryset(self, request, pk):
        color = request.data.get('color', '')
        material = request.data.get('material', '')
        size = request.data.get('size', '')
        return Stock.objects.get(product_meta__product__pk=pk,
                                 product_meta__color__name=color,
                                 size__name=size,
                                 product_meta__material__name=material)

    def delete(self, request, pk):
        color = request.data.get('color', '')
        material = request.data.get('material', '')
        size = request.data.get('size', '')
        stock = Stock.objects.get(product_meta__product__pk=pk,
                                  product_meta__color__name=color,
                                  size__name=size,
                                  product_meta__material__name=material)
        if not stock.exists():
            return Response(status=HTTP_404_NOT_FOUND)
        stock.delete()
        return Response(status=HTTP_204_NO_CONTENT)

    def update(self, request, pk):
        color = request.data.get('color', '')
        material = request.data.get('material', '')
        size = request.data.get('size', '')
        value = request.data.get('value', 0)

        stock = Stock.objects.get(product_meta__product__pk=pk,
                                  product_meta__color__name=color,
                                  size__name=size,
                                  product_meta__material__name=material)
        if not stock.exists():
            return Response(status=HTTP_404_NOT_FOUND)
        if stock.quantity < value:
            return Response("No sufficient product in stock, try later", status=HTTP_400_BAD_REQUEST)
        stock.quantity -= value
        serializer = StockSerializer(stock, data=stock.__dict__)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(status=HTTP_400_BAD_REQUEST)

    def update_stock(self, stock, quantity):
        stock.quantity -= quantity
        s_serializer = StockSerializer(stock, data=stock.__dict__)
        if s_serializer.is_valid():
            s_serializer.save()

@permission_classes((AllowAny,))
class StockView(APIView):
    serializer_class = StockSerializer

    def get_queryset(self):
        return Stock.objects.filter(quantity__gt=0)


class OrderView(APIView):
    queryset = Order.objects.filter(deleted_date=None)
    serializer_class = OrderSerializer

    def perform_create(self, serializer):
        if self.request.data.get('product', None) is None:
            return Response("Product is required", status=HTTP_400_BAD_REQUEST)
        if self.request.data.get('address', None) is None:
            return Response("Address is required", status=HTTP_400_BAD_REQUEST)
        if self.request.data.get('quantity', 0) == '':
            return Response("Quantity is required", status=HTTP_400_BAD_REQUEST)
        stock = Stock.objects.filter(pk=self.request.data.get('product').get('pk'))
        if stock.exists():
            return Response("Product doesn't exists", status=HTTP_400_BAD_REQUEST)
        quantity = self.request.data.get('quantity')
        if stock.quantity < quantity:
            return Response("We have in stock only " + stock.quantity + " products of type " +
                            self.request.data.get('product', None), HTTP_400_BAD_REQUEST)
        StockDetailView.update_stock(stock, quantity)
        serializer.save(user=self.request.user,
                        product=self.request.data.get('product'),
                        address=self.request.data.get('address'),
                        quantity=self.request.data.get('quantity'),
                        created_date=timezone.now())
        return Response(status=HTTP_201_CREATED)

    def get_objects(self):
        try:
            return Order.objects.filter(user=self.request.user,
                                        deleted_date=None, product__deleted_date=None)
        except InvalidOperation:
            raise Http404

    def get(self):
        orders = self.get_objects()
        serializer = OrderSerializer(orders, many=True)
        return Response(serializer.data)


class CartProductView(APIView):
    queryset = CartProduct.objects.all()
    serializer_class = CartProductSerializer

    def perform_create(self, serializer):
        if self.request.data.get('product', None) is None:
            return Response("Product is required", status=HTTP_400_BAD_REQUEST)
        if self.request.data.get('quantity', 0) == 0 or self.request.data.get('quantity') < 0:
            return Response("Quantity should be greater than 0", status=HTTP_400_BAD_REQUEST)
        product = self.request.data.get('product')
        exists = CartProduct.objects.filter(product_meta__pk=product.get('pk'), user=self.request.user)
        quantity = self.request.data.get('quantity')
        if quantity < 0:
            return Response(HTTP_400_BAD_REQUEST)
        if exists.exists():
            exists[0].quantity += quantity
            serializer = CartProductSerializer(exists[0], data=exists[0].__dict__)
            if serializer.is_valid():
                serializer.save()
        else:
            cart_product = CartProduct()
            cart_product.user = self.request.user
            cart_product.product = product
            cart_product.added_date = timezone.now()
            cart_product.quantity = quantity
            cart_product.save()

    def get_objects(self):
        try:
            return CartProduct.objects.filter(user=self.request.user)
        except InvalidOperation:
            raise Http404

    def get(self):
        cart_products = self.get_objects()
        serializer = CartProductSerializer(cart_products, many=True)
        return Response(serializer.data)


class ReviewView(CreateView):
    serializer_class = ReviewSerializer
    model = Review

    def perform_create(self, serializer):
        if self.request.data.get('product', None) is None:
            return Response("Product is required", status=HTTP_400_BAD_REQUEST)
        product = self.request.data.get('product')
        size = self.request.data.get('size')
        height = self.request.data.get('height')
        weight = self.request.data.get('weight')
        bust = self.request.data.get('bust')
        waist = self.request.data.get('waist')
        hips = self.request.data.get('hips')

        exists = Review.objects.filter(product__pk=product.get('pk'), user=self.request.user)

        if exists.exists():
            return Response(HTTP_404_NOT_FOUND)
        else:
            review = Review()
            review.user = self.request.user
            review.size = size
            review.added_date = timezone.now()
            review.height = height
            review.weight = weight
            review.bust = bust
            review.waist = waist
            review.hips = hips

            review.save()

    def get_objects(self):
        try:
            return Review.objects.filter(user=self.request.user, product__pk=self.request.data.get("product_pk"),
                                         size__name=self.request.data.get("name"))
        except InvalidOperation:
            raise Http404

    def get(self):
        reviews = self.get_objects()
        with open(self.request.user.pk + self.request.data.get("product")+'.csv', 'w', newline='') as csvfile:
            rew_writer = csv.writer(csvfile, delimiter=' ',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            rew_writer.writerow('height','weight','bust','waist','hips','size')
            for review in reviews:
                rew_writer.writerow(review.height,review.weight,review.bust,review.waist,review.hips,review.size.name)
        url = self.request.user.pk + self.request.data.get("product")+'.csv'
        names = ['height','weight','bust','waist','hips','size']
        dataset = pandas.read_csv(url, names=names)
        data, target = dataset[1:,0:len(dataset[0]) - 1],  numpy.squeeze(dataset[1:,len(dataset[0]) - 1:])
        classifier = svm.SVC(gamma=0.001, C=100.0)
        classifier.fit(data, target)
        height = self.request.data.get('height')
        weight = self.request.data.get('weight')
        bust = self.request.data.get('bust')
        waist = self.request.data.get('waist')
        hips = self.request.data.get('hips')
        size = classifier.predict([[height,weight,bust,waist, hips]])
        return Response(size)
