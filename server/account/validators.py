from rest_framework.exceptions import ValidationError


def validate_height(value):
    if value > 220:
        raise ValidationError('Height can\'t be larger than 220')
    if value < 100:
        raise ValidationError('Height can\'t be smaller than 100')


def validate(value):
    if value > 220:
        raise ValidationError('Value can\'t be larger than 220')
    if value < 40:
        raise ValidationError('Value can\'t be smaller than 20')
