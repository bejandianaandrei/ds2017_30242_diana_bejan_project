from __future__ import unicode_literals

from django.contrib import admin
from account.models import User, Address, Category, Color, Material, Size, Product, ProductMeta, \
    Stock, Order, CartProduct, Review

admin.site.register(User)
admin.site.register(Address)
admin.site.register(Category)
admin.site.register(Color)
admin.site.register(Material)
admin.site.register(Size)
admin.site.register(Product)
admin.site.register(ProductMeta)
admin.site.register(Stock)
admin.site.register(Order)
admin.site.register(CartProduct)
admin.site.register(Review)
