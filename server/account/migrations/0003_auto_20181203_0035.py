# Generated by Django 2.1.3 on 2018-12-02 22:35

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_auto_20181202_2203'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='category',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='product_category', to='account.Category'),
        ),
        migrations.AddField(
            model_name='product',
            name='metas',
            field=models.ManyToManyField(related_name='product_meta', to='account.ProductMeta'),
        ),
        migrations.AlterField(
            model_name='cartproduct',
            name='added_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 12, 2, 22, 35, 31, 325627, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='category',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 12, 2, 22, 35, 31, 322389, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='order',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 12, 2, 22, 35, 31, 325017, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='product',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 12, 2, 22, 35, 31, 322748, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='review',
            name='added_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 12, 2, 22, 35, 31, 326209, tzinfo=utc)),
        ),
    ]
