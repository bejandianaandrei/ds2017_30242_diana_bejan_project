# Generated by Django 2.1.3 on 2018-12-06 13:30

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0004_auto_20181204_2343'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cartproduct',
            name='added_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 12, 6, 13, 30, 41, 606925, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='category',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 12, 6, 13, 30, 41, 604450, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='order',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 12, 6, 13, 30, 41, 606370, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='product',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 12, 6, 13, 30, 41, 604833, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='review',
            name='added_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 12, 6, 13, 30, 41, 607409, tzinfo=utc)),
        ),
    ]
