from account import views
from django.conf.urls import url

from account.views import imageView

app_name = 'account'

urlpatterns = [
    url(r'^product_pictures/(?P<imagePath>[^/]+)/?$', imageView, name="Image"),
    url(r'users/$', views.UserList.as_view(), name='user-list'),
    url(r'active_users/$', views.ActiveUserList.as_view(), name='active-user-list'),
    url(r'users/(?P<pk>[0-9]+)/$', views.UserDetail.as_view(), name='user-detail'),
    url(r'address/(?P<pk>[0-9]+)/$', views.AddressDetailView.as_view(), name='address-detail'),
    url(r'address/$', views.AddressView.as_view(), name='address-list'),
    url(r'category/$', views.CategoryView.as_view(), name='category-view'),
    url(r'category/(?P<pk>[0-9]+)/$', views.CategoryDetailView.as_view(), name='category-detail'),
    url(r'color/$', views.ColorView.as_view(), name='color-view'),
    url(r'color/(?P<pk>[0-9]+)/$', views.ColorDetailView.as_view(), name='color-detail'),
    url(r'size/$', views.SizeView.as_view(), name='size-view'),
    url(r'size/(?P<pk>[0-9]+)/$', views.SizeDetailView.as_view(), name='size-detail'),
    url(r'material/$', views.MaterialView.as_view(), name='material-view'),
    url(r'material/(?P<pk>[0-9]+)/$', views.MaterialDetailView.as_view(), name='material-detail'),
    url(r'product/(?P<category>[^/]+)/(?P<name_filter>[^/]+)/(?P<price_min>[0-9]+(\.[0-9]*)?)/(?P<price_max>[0-9.]('
        r'\.[0-9]*)?)/?$', views.ProductListView.as_view(), name='product-list-view'),
    url(r'product/(?P<pk>[0-9]+)/$', views.ProductDetailView.as_view(), name='product-view'),
    url(r'product_meta/(?P<category>[^/]+)/(?P<color>[^/]+)/(?P<material>[^/]+)/'
        r'/?$', views.ProductMetaListView.as_view(), name='product-meta-list-view'),
    url(r'product_meta/(?P<product_id>[0-9]+)/$', views.ProductMetaDetailView.as_view(), name='product-meta-detail-view'),
    url(r'stock/$', views.StockView.as_view(), name='stock-view'),
    url(r'stock/(?P<pk>[0-9]+)/$', views.StockDetailView.as_view(), name='stock-detail'),
    url(r'order/$', views.OrderView.as_view(), name='order-view'),
    url(r'cart/$', views.CartProductView.as_view(), name='cart-view'),
    url(r'review/$', views.ReviewView.as_view(), name='review-view'),
]
