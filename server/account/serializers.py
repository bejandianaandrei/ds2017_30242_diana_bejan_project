import datetime

from rest_framework import serializers

from account.models import User, Address

from account.models import Category, Color, Size, Material, Product, ProductMeta, Order, Stock, CartProduct, \
    Review


class UserSerializer(serializers.HyperlinkedModelSerializer):

    def update(self, instance, validated_data):
        for field in validated_data:
            if field == 'password':
                instance.set_password(validated_data.get(field))
            else:
                instance.__setattr__(field, validated_data.get(field))
        instance.save()
        return instance

    class Meta:
        model = User
        fields = (   'id',
                     'username',
                     'password',
                     'first_name',
                     'last_name',
                     'birthday',
                     'registration_date',
                     'email',
                     'deleted_date',
                     'photo',
                    )

        read_only_fields = (
                             'id',
                             'username',
                             'password',
                             'first_name',
                             'last_name',
                             'birthday',
                             'registration_date',
                             'email',
                             'deleted_date',
                             'photo',
                            )
        extra_kwargs = {
            'url': {
                'view_name': 'account:user-detail',
            }
        }


class BriefUserSerializer(serializers.Serializer):
    username = serializers.CharField()
    id = serializers.IntegerField()
    token = serializers.CharField(read_only=True, required=False)


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = (
            'id',
            'user',
            'country',
            'city',
            'address',
        )
        read_only_fields = (
            'id',
            'user',
            'country',
            'city',
            'address',
        )

        extra_kwargs = {
            'url': {
                'view_name': 'tracker:address',
            }
        }


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = (
            'id',
            'name',
            'created_date',
            'deleted_date',
        )
        read_only_fields = (
            'id',
            'name',
            'created_date',
            'deleted_date',
        )
        extra_kwargs = {
            'url': {
                'view_name': 'tracker:category',
            }
        }


class ColorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Color
        fields = (
            'id',
            'name',
            'value',
        )
        read_only_fields = (
            'id',
            'name',
            'value',
        )


class SizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Size
        fields = (
            'id',
            'name',
            'value',
        )
        read_only_fields = (
            'id',
            'name',
            'value',
        )


class MaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Material
        fields = (
            'id',
            'name',
            'description',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
        )


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = (
            'id',
            'product_picture',
            'name',
            'category',
            'description',
            'price',
            'original_price',
            'created_date',
            'deleted_date',
        )
        read_only_fields = (
            'id',
            'product_picture',
            'name',
            'category',
            'description',
            'price',
            'original_price',
            'created_date',
            'deleted_date',
        )


class ProductMetaSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductMeta
        fields = (
            'id',
            'photo',
            'color',
            'material',
            'product',
        )
        read_only_fields = (
            'id',
            'photo',
            'color',
            'material',
            'product',
        )


class StockSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stock
        fields = (
            'id',
            'product_meta',
            'size',
            'quantity',
        )
        read_only_fields = (
            'id',
            'product_meta',
            'size',
            'quantity',
        )


class OrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = (
            'id',
            'user',
            'product',
            'address',
            'quantity',
            'created_date',
            'deleted_date',
        )


class CartProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = CartProduct
        fields = (
            'id',
            'user',
            'product',
            'stock',
            'added_date',
            'quantity',
        )


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = (
            'id',
            'user',
            'product',
            'size',
            'added_date',
            'height',
            'weight',
            'bust',
            'waist',
            'hips',
        )

