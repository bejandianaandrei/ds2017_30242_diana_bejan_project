# Project setup
#### 1 clone the project locally
#### 2 if you don't have pyhton3 installed run 
##### ``` brew install python3 ```
#### 3 install pip
##### ``` python3 -m pip install --upgrade pip```
#### 4 run the command to activate the virtual environement 
##### ```source env/bin/activate```
#### 5 to install the dependencies run
##### ```pip install -r requirements.txt```
#### 6 create database by running
##### ```python3 manage.py migrate```
#### 7 create an admin by running
##### ```python manage.py createsuperuser```
 